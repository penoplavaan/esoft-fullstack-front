import React, {useState} from 'react';
import {Accordion} from "react-bootstrap";
import RangeSlider from "./RangeSlider";
import {useLocation} from "react-router-dom";
import $ from "jquery";
import Apartment from "./Apartment";



export default class Controls extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            apartments: [],
            maxFloor: 1,
            itemsCount: 0,
            minPrice: 1000000000,
            maxPrice: 0,
            apartmentList: []
        }
    }

    componentDidMount() {
        this.fetch();
    }

    fetch() {
        const context = this;
        $.ajax({
            url: `http://localhost:8080`,
            method: 'GET',
            success: function(response) {
                let children = [];
                for (const responseKey in response) {
                    children.push(response[responseKey]);
                }
                context.setState({
                    apartments: children,
                });
            }
        }).then(()=>{
            if(this.state.apartments.length){
                let apartments = this.state.apartments;

                let localMin = this.state.minPrice;
                let localMax = this.state.maxPrice;
                let apartmentsCount = apartments.length;

                apartments.forEach((apartment)=>{
                    localMin = apartment.price < localMin ? apartment.price : localMin;
                    localMax = apartment.price > localMax ? apartment.price : localMax;
                })
                this.setState(
                    {
                        minPrice: localMin,
                        maxPrice: localMax,
                        itemsCount: apartmentsCount
                    }
                )
            }

        })

    }

    render() {
        if (this.state.minPrice !== 1000000000 && this.state.maxPrice !== 0 && this.state.maxPrice !==this.state.minPrice){
            return (
                <div className="col">
                    <h2>Выбор квартир</h2>
                    <div className="row">
                        <div className="col">
                            <Accordion  >
                                <Accordion.Item eventKey="1">
                                    <div className="row">
                                        <div className="col-8">
                                            <div className="row">
                                                <div className="col">
                                                    <div className="row">
                                                        <div className="col">
                                                            Количество комнат
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col">
                                                            1, 2, 3
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="col">
                                                    <div className="row">
                                                        <div className="col">
                                                            Стоимость
                                                        </div>
                                                    </div>
                                                    <div className="row">
                                                        <div className="col">
                                                            <RangeSlider
                                                                min= {this.state.minPrice}
                                                                max= {this.state.maxPrice}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-4">
                                            <span>Всего найдено: {this.state.itemsCount}</span>
                                            <Accordion.Header>Расширенный фильтр</Accordion.Header>
                                        </div>
                                    </div>
                                    <Accordion.Body>
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod
                                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim
                                        veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                        commodo consequat. Duis aute irure dolor in reprehenderit in voluptate
                                        velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                                        cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id
                                        est laborum.
                                    </Accordion.Body>
                                </Accordion.Item>
                            </Accordion>
                        </div>
                    </div>
                </div>
            )
        }
        return (
            <div className="col"/>
        )

    }

}