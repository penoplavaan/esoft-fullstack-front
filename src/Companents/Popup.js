import React, {useState} from 'react';
import Modal from 'react-bootstrap/Modal'
import Card from "react-bootstrap/Card";
import styles from './css/popup.css'
import Button from "react-bootstrap/Button"

function calculateRotateAngle(posOnFloor){
    if(posOnFloor === 1) return 'compass-rotate180'
    if(posOnFloor === 5) return 'compass-rotate0'
    if(posOnFloor === 6) return 'compass-rotate-90'
    return 'compass-rotate90'
}

function ModalExample(props) {
    const [show, setShow] = useState(false);
    return (<div className="modal-example">
            <button
                style={{"border": "none", "padding": "0"}}
                onClick={() => setShow(true)}
            >
                <Card.Img variant="top" src={props.imagesrc}/>
            </button>


            <Modal
                {...props}
                size="xl"
                aria-labelledby="contained-modal-title-vcenter"
                centered
                show={show}
                onHide={() => setShow(false)}
            >
                <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                        {props.rooms}-к квартира, id: {props.id}
                    </Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <div className="container">
                        <div className="row">
                            <div className="col-sm-6">
                                <img  src={props.imagesrc} alt="" style={{'width':"100%"}}/>
                            </div>
                            <div className="col-sm-2">
                                <img src={process.env.PUBLIC_URL + '/img/'+props.pos_on_floor+".jpg"} className={'pos-on-floor-img'}/>
                                <img src={process.env.PUBLIC_URL + '/img/compass.svg'} alt="" className={"compass "+calculateRotateAngle(props.pos_on_floor)}/>
                            </div>
                            <div className="col-sm-4">
                                <div className="row">
                                    <div className="col">
                                        Дом: {props.building_number}
                                    </div>
                                    <div className="col">
                                        Этаж: {props.floor} из {props.max_floor}
                                    </div>
                                    <div className="col">
                                        Площадь: {props.area_total}м2
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <h4>Особенности планировки:</h4>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        Жилая площадь: {props.area_live}м2
                                    </div>
                                    <div className="col">
                                        Площадь кухни: {props.area_kitchen}м2
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col">
                                        <h3>
                                            {props.price} руб.
                                        </h3>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </Modal.Body>


            </Modal>
        </div>);
}

export default ModalExample